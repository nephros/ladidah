#!/bin/bash

if [ -z "$BASH_VERSION" ]; then
	echo "GNU bash needed. You seem to be running busybox bash"
	exit 1
fi
rate=22050
dit_hz=600
dah_hz=800

outdir=qml/sounds
#for len in 1200 600 400 300 240 200 171 150 138 133 120 109 100 92 85 80 75 70 66 63 60 57 54 52; do
for wpm in {1..13}; do
	len=$((1200/${wpm}))
	for what in dit dah; do
		hz=$dit_hz
		if [ $what = dah ]; then
			hz=$dah_hz
			len=$(( $len * 3 ))
		fi
		flen=0.${len}
		if [ $len -ge 1000 ]; then
			flen=${len:0:1}.${len:1:3}
		fi
		if [ $len -lt 100 ]; then
			flen=0.0${len}
		fi
		sox -n -b 16 -r $rate "${outdir}"/${what}_${len}.wav synth ${flen} sine ${hz}
	done
done
