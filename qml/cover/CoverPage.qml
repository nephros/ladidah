import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

CoverBackground {
    id: coverPage

    /*
    Image {
        source: "./background.png"
        z: -1
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
        }
        sourceSize.width: parent.width
        fillMode: Image.PreserveAspectFit
        opacity: 0.2
    }
    */

    CoverPlaceholder {
    Label {
        text: Qt.application.name
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: Theme.itemSizeSmall/2
    }
    Fresnel {
        label: false
        //anchors.centerIn: parent
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height * 4/5
    }
    /*
    CoverPlaceholder {
        text: Qt.application.name
        textColor: Theme.highlightColor
        icon.source: "image://theme/harbour-laditdah"
        */

        CoverActionList {
            CoverAction { iconSource: "image://theme/icon-m-play";   onTriggered: { blinker.start()} }
            CoverAction { iconSource: "image://theme/icon-m-repeat"; onTriggered: { app.loop = !app.loop } }
        }
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
