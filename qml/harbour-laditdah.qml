/*

Apache License 2.0

Copyright (c) 2022 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the
License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import QtMultimedia 5.6
import Sailfish.Silica 1.0
//import Nemo.Configuration 1.0
import com.jolla.settings.system 1.0

import "pages"
import "cover"
import "components"
import "js/morsetable.js" as CodeBook

ApplicationWindow {
    id: app

    property bool sending: false
    property var morseCode

    /* detect closing of app, so we can turn off the light */
    signal willQuit()
    property bool quitting: false
    Connections { target: __quickWindow; onClosing: willQuit() }
    onWillQuit: {
        // signal is received twice.
        if (quitting) return;
        if (flashlight.flashlightOn) { flashlight.toggleFlashlight() }
        quitting = true
    }

    Component.onCompleted: {
        // for sailjail
        Qt.application.domain  = "sailfish.nephros.org";
        Qt.application.version = "unreleased";
        console.info("Intialized", Qt.application.name, "version", Qt.application.version, "by", Qt.application.organization );
        console.debug("Parameters: " + Qt.application.arguments.join(" "))
        // load config
        morseCode = CodeBook.table;
    }

    /*
    // application settings:
    ConfigurationGroup  {
        id: settings
        path: "/org/nephros/" + Qt.application.name
    }
    ConfigurationGroup  {
        id: config
        scope: settings
        path:  "app"
        //property bool gravity: true // true: pull to bottom
        //property int ordering: 1 // 1: top to bottom
    }
    */

    /* Globals */
    property bool silent: false
    property bool visual: false
    property bool loop: false
    readonly property string ditSymbol: "·" // center dot
    readonly property string dahSymbol: "–" // en dash
    //readonly property string dahSymbol: " ― " // horizontal bar
    // 1 WpM === 50 dits per minute
    property int wpm: 5
    property int ditLength:   Math.floor(1200 / wpm)
    property int dahLength:   Math.floor(3 * ditLength)
    property int symbolPause: Math.floor(1 * ditLength)
    property int letterPause: Math.floor(3 * ditLength)
    property int wordPause:   Math.floor(7 * ditLength)

    SoundEffect { id: ditSound; source: "sounds/dit_" + ditLength + ".wav" }
    SoundEffect { id: dahSound; source: "sounds/dah_" + dahLength + ".wav" }

    Blinker { id: blinker }

    Flashlight { id: flashlight }
    /* alternatively go through dbus:
    DBusInterface {
        id: flashlight

        bus: DBus.SessionBus
        service: "com.jolla.settings.system.flashlight"
        path: "/com/jolla/settings/system/flashlight"
        iface: "com.jolla.settings.system.flashlight"

        function toggle() {
            torch.call("toggleFlashlight");
            return;
        }
    }
    */

    initialPage: Component { MainPage{} }
    cover: CoverPage{}

    function wordToMorse(str) {
        const s = str;
        return s.split("").map(
            function(el) {
                return morseCode[el] ? morseCode[el].replace(/\./g, ditSymbol).replace(/-/g, dahSymbol) : el;
            });
    }
    function toMorse(str) {
        const words = str.split(" ");
        //console.debug("got words:", words);
        const morse = [];
        for (var i = 0; i < words.length; i++) {
            morse.push(wordToMorse(words[i]));
            //console.debug("translated ", words[i], " --> ", morse[i]);
        }
        //console.debug("returning morse:", JSON.stringify(morse));
        return morse;
    }
}

// vim: ft=javascript expandtab ts=4 sw=4 st=4
