/*

Apache License 2.0

Copyright (c) 2022 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import QtQml.StateMachine 1.0

StateMachine { id: machine
    property int pauseDuration: 0
    property var queue: []
    property var queueStr: []
    property bool signalling: false

    readonly property string letterPauseSymbol: "|"
    readonly property string wordPauseSymbol: ":"

    initialState: idleState
    // start the state machine
    running: true

    signal ditS
    signal dahS
    signal pauseS
    signal backS

    // deconstruct the nice array of arrays into a string again ;)
    function q2qs() {
        if (!queue) return;
        //console.debug("builing queue string from input", JSON.stringify(queue));
        const q = "";
        if ( (queue.length === 1) && (!queue[0][0]) ) { // a single symbol
                q = queue[0]
        } else {
            for (var i = 0; i < queue.length; i++) {            // handle words
                for (var j = 0; j < queue[i].length; j++ ) {    // handle letters
                    q = q +  queue[i][j] + letterPauseSymbol;
                }
                q = q + wordPauseSymbol;
            }
        }
        //console.debug("built queue string:", q, "\n from input", JSON.stringify(queue));
        queueStr = q.split("");
    }
    onQueueChanged: {
        //console.debug("q changed:", queue)
        if (running && loop) return;
        q2qs();
        start();
    }
    onStopped: {
        if (loop) { q2qs(); start() } else {
            //queue = []
            sending = false
            //if (flashlight.flashlightOn) { flashlight.toggleFlashlight() }
            console.debug("The Machine Stops. (E. M. Forster 1909)")
        }
    }

    HistoryState { id: historyState ; defaultState: pausedState }
    SignalTransition { targetState: historyState; signal: backS }

    State { id: idleState
        onEntered: {
            //console.debug("idle entered")
            sending = false
            if (queueStr.length > 0) {
                const next = queueStr.shift();
                //console.debug("q has next:", next)
                switch (next) {
                    case ditSymbol: ditS(); break;
                    case dahSymbol: dahS(); break;
                    case letterPauseSymbol:
                        pauseDuration = letterPause - symbolPause; // every symbol pauses
                        pauseS();
                        break;
                    case wordPauseSymbol:
                        pauseDuration = wordPause - ( letterPause + symbolPause); // every symbol pauses
                        pauseS();
                        break;
                    default:
                        console.warn("unhandled symbol in letter")
                        break;
                }
            } else {
                //console.debug("q is empty")
                machine.stop();
            }
        }
        onExited: { sending = true }
        SignalTransition { targetState: ditState; signal: machine.ditS }
        SignalTransition { targetState: dahState; signal: machine.dahS }
        SignalTransition { targetState: pausedState; signal: machine.pauseS }
    }
    State { id: ditState
        initialState: ditOnState
        State { id: ditOnState
            onEntered: {
                machine.signalling = true
                if (visual) flashlight.toggleFlashlight()
                if (!silent) ditSound.play()
            }
            TimeoutTransition { targetState: ditOffState ; timeout: ditLength}
        }
        State { id: ditOffState
            onEntered: {
                if (visual) flashlight.toggleFlashlight()
                machine.signalling = false
            }
            TimeoutTransition { targetState: ditPauseState ; timeout: 0} // go to pause immediately
        }
        State { id: ditPauseState
            //onEntered: console.debug("symbol pause:", Math.floor(symbolPause/ditLength), "dits")
            TimeoutTransition { targetState: idleState ; timeout: symbolPause } // TODO: do not pause at the end of a word!
        }
    }
    State { id: dahState
        initialState: dahOnState
        State { id: dahOnState
            onEntered: {
                machine.signalling = true
                if (visual) flashlight.toggleFlashlight()
                if (!silent) dahSound.play()
            }
            TimeoutTransition { targetState: dahOffState ; timeout: dahLength}
        }
        State { id: dahOffState
            onEntered: {
                if (visual) flashlight.toggleFlashlight()
                machine.signalling = false
            }
            TimeoutTransition { targetState: dahPauseState ; timeout: 0} // go to pause immediately
        }
        State { id: dahPauseState
            //onEntered: console.debug("symbol pause:", Math.floor(symbolPause/ditLength), "dits")
            TimeoutTransition { targetState: idleState ; timeout: symbolPause } // TODO: do not pause at the end of a word!
        }
    }
    State { id: pausedState
        onEntered: {
            if (!pauseDuration || pauseDuration === 0 ) console.warn("Pause without valid duration")
            //console.debug("other pause:", Math.floor(pauseDuration/ditLength), "dits")
        }
        onExited: { pauseDuration = 0 }
        TimeoutTransition { targetState: idleState ; timeout: machine.pauseDuration}
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
