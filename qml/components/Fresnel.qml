/*

Apache License 2.0

Copyright (c) 2022 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import QtGraphicalEffects 1.0
import Sailfish.Silica 1.0

Rectangle {
    property bool active: blinker.signalling
    property bool label: true
    anchors.horizontalCenter: parent.horizontalCenter
    width: height
    //height: Theme.itemSizeHuge*2
    //color: "white"
    //color: "black"
    color: "transparent"
    //opacity: active ? 1.0 : 0.2
    GlassItem{
        dimmed: !parent.active
        width:parent.width
        height:width
        palette.backgroundGlowColor: parent.active ? "white" : Theme.backgroundGlowColor
        //pattern: Qt.resolvedUrl("../images/blinkerpattern.png")
        Image { id: fresnel
            source: Qt.resolvedUrl("../images/fresnel.png")
            opacity: 0.2
            anchors.centerIn: parent
            height: parent.height/2.2
            width: height
            sourceSize.width: width
            sourceSize.height: height
            layer.enabled: true
            layer.effect: OpacityMask { maskSource: mask }
        }
        Rectangle { id: mask
            visible: false
            anchors.centerIn: fresnel
            height: fresnel.height - 50
            width: height
            radius: width / 2
        }
    }
    Label {
        visible: parent.label
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        text: sending ? (qsTr("Sending.") + "\n" + qsTr("Tap to enter fullscreen")) : qsTr("idle")
        font.pixelSize: Theme.fontSizeSmall
        color: Theme.secondaryColor
        horizontalAlignment: Qt.AlignHCenter
    }
    BackgroundItem {
        anchors.fill: parent
        onClicked: { pageStack.push(Qt.resolvedUrl("../pages/BlinkDialog.qml")) }
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
