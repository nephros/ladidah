/*

Apache License 2.0

Copyright (c) 2022 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0
//import com.jolla.settings 1.0
import org.nemomobile.systemsettings 1.0

Page {
    id: page
    property int currentBrightness
    DisplaySettings { id: dispSettings }
    onStatusChanged: {
        if (status === PageStatus.Activating ) {
            if (!currentBrightness) currentBrightness = dispSettings.brightness
            console.debug("saved brightness" , currentBrightness)
            dispSettings.brightness = dispSettings.maximumBrightness
            //dispSettings.inhibitMode = DisplaySettings.InhibitStayOn
        } else if (status === PageStatus.Inactive)  {
            console.debug("setting brightness to" , currentBrightness)
            // TODO: this ends up about twice as bright as originally set??
            dispSettings.brightness = currentBrightness
            //dispSettings.inhibitMode = false
        }
    }
    Rectangle {
        anchors.fill: parent
        color: blinker.signalling  ? "white" : "black"
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
