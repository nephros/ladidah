/*

Apache License 2.0

Copyright (c) 2022 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    id: page

    property Text logbuf: Text{}
    ListModel { id: record }

    function dit() { logbuf.text = logbuf.text + ditSymbol; recordTimer.restart() ; blinker.queue = [ditSymbol,] ; }
    function dah() { logbuf.text = logbuf.text + dahSymbol; recordTimer.restart() ; blinker.queue = [dahSymbol,] ; }

    Timer { id: recordTimer
        interval: wordPause
        repeat: false
        //running: true
        onTriggered: {
            record.append( { "word": logbuf.text })
            logbuf.text = ""
        }
    }
    SilicaFlickable {
        anchors.centerIn: parent
        anchors.fill: parent
        PageHeader { id: head ; title: qsTr("Morse Manually"); anchors.top: parent.top }
        Flow {
            anchors.top: head.bottom
            width: parent.width
            height: parent.height - head.height
            ListView { id: log
                clip: true
                width:  page.isLandscape ? parent.width/3 :  parent.width
                height: page.isLandscape ? buttons.height : parent.height/3
                //anchors.leftMargin: page.isPortrait ? Theme.itemSizeSmall : Theme.paddingSmall
                //anchors.rightMargin: page.isPortrait ? Theme.itemSizeSmall : Theme.paddingSmall
                //highlight: Component { Rectangle { anchors.centerIn: log.currentItem; opacity: 0.3 ; color: Theme.highlightBackgroundColor; border.color: Theme.highlightColor; border.width: 1 } }
                highlightFollowsCurrentItem : true
                //verticalLayoutDirection: ListView.BottomToTop
                currentIndex: record.count -1
                model: record
                delegate: ListItem {
                    width: ListView.view.width - Theme.itemSizeLarge
                    //height: Theme.itemSizeSmall
                    anchors.horizontalCenter: parent.horizontalCenter
                    Label { id: lbl
                        anchors.centerIn: parent
                        font.pixelSize: Theme.fontSizeHuge
                        truncationMode: TruncationMode.Fade
                        wrapMode: Text.WrapAnywhere
                        color: Theme.secondaryColor
                        text: word ? word : ""
                    }
                    onClicked: blinker.queue = toMorse(word);
                }
                footerPositioning: ListView.OverlayFooter
                footer: ListItem {
                    z: 10
                    clip: true
                    width: ListView.view.width - Theme.itemSizeSmall
                    height: Theme.itemSizeExtraSmall
                    anchors.horizontalCenter: parent.horizontalCenter
                    Label {
                        anchors.centerIn: parent
                        font.pixelSize: Theme.fontSizeLarge
                        font.bold: true
                        truncationMode: TruncationMode.Fade
                        text: logbuf.text
                    }
                }
            }
            Row { id: buttons
                width:  page.isLandscape ? parent.width/3 : parent.width
                height: page.isLandscape ? parent.height : parent.height/3
                spacing: Theme.paddingLarge
                BackgroundItem { id: ditButton
                    anchors {
                        verticalCenter: parent.verticalCenter
                        right: parent.horizontalCenter
                        rightMargin: Theme.paddingLarge
                        //leftMargin: Theme.horizontalPageMargin
                    }
                    width: Screen.width/2.5 - Theme.paddingLarge
                    height: width
                    _showPress: false
                    highlighted: false
                    Rectangle {
                        anchors.fill: parent
                        border.color: Theme.secondaryColor
                        color: ditButton.down
                            ? Theme.highlightBackgroundColor
                            : Theme.highlightDimmerFromColor(Theme.highlightDimmerColor, Theme.colorScheme)
                        border.width: 4
                        radius: width/2
                    }
                    Rectangle { color: Theme.secondaryColor; anchors.centerIn: parent; height: Theme.itemSizeSmall; width: height; radius: width/2}
                    onClicked: dit()
                }
                BackgroundItem { id: dahButton
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.horizontalCenter
                        leftMargin: Theme.paddingLarge
                        //rightMargin: Theme.horizontalPageMargin
                    }
                    width: Screen.width/2.5 - Theme.paddingLarge
                    height: width
                    _showPress: false
                    highlighted: false
                    Rectangle {
                        anchors.fill: parent
                        border.color: Theme.secondaryColor
                        color: dahButton.down
                            ? Qt.darker(Theme.highlightDimmerFromColor(Theme.highlightDimmerColor, Theme.colorScheme))
                            : Theme.highlightDimmerFromColor(Theme.highlightDimmerColor, Theme.colorScheme)
                        border.width: 4
                        radius: width/2
                    }
                    Rectangle { color: Theme.secondaryColor; anchors.centerIn: parent; height: Theme.itemSizeSmall/2; width: height*3; radius: width/2}
                    onClicked: dah()
                }
            }
            Flow { id: snippets
                width:  page.isLandscape ? parent.width/3 :  parent.width
                height: page.isLandscape ? buttons.height : Theme.itemSizeLarge
                topPadding:  Theme.itemSizeMedium
                leftPadding: Theme.paddingLarge
                rightPadding: Theme.paddingLarge
                spacing: Theme.paddingLarge
                Repeater {
                    model: snippetModel
                    delegate: SecondaryButton {
                        preferredWidth: Theme.buttonWidthExtraSmall
                        //text: name + (plain ? "\n" + plain : ( morse ? " " + morse : "???" ))
                        text: morse ? morse : ( plain ? plain : "inv" )
                        onClicked: {
                            if (morse.length > 0){
                                record.append( { "word": morse } )
                                blinker.queue = [morse.replace(/\./g,ditSymbol).replace(/-/g,dahSymbol),]
                            } else if (plain.length > 0) {
                                var m = toMorse(plain);
                                record.append( { "word": m });
                                blinker.queue = m;
                            }
                        }
                    }
                }
                ViewPlaceholder{
                    anchors.fill: parent
                    enabled: snippetModel.count <= 1
                    text: qsTr("You can place custom words here.")
                    //hintText: qsTr("Refer to the documentation on how.\n(but it's placing a file at %1").arg(StandardPaths.data + "/" +"snippets.json")
                    hintText: qsTr("by placing a file at %1").arg(StandardPaths.data + "/" +"snippets.json")
                }
            }
        }
    }
    ListModel { id: snippetModel}
    function fillModel(data) {
            //console.debug("data" , JSON.stringify(data))
        for (var i = 0; i < data["snippets"].length; i++) {
            //console.debug("appending" , data["snippets"][i])
            snippetModel.append(data["snippets"][i]);
        }
    }
    Component.onCompleted: {
        var storagePath = StandardPaths.data
        var filename = "snippets.json"
        var url = Qt.resolvedUrl(storagePath + "/" + filename)
        console.info("Trying to load a snippet file from", url)
        var r = new XMLHttpRequest()
        r.open('GET', url);
        r.setRequestHeader('User-Agent', "Mozilla/5.0 (Sailfish; Mobile) " + Qt.application.name + "/" + Qt.application.version);
        r.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        r.send();

        r.onreadystatechange = function(event) {
          if (r.readyState == XMLHttpRequest.DONE) {
              const payload = r.response;
              //console.debug("got from xhr:", payload)
              try {
                  fillModel(JSON.parse(payload));
              } catch (e) {
                  console.info("Snippet file loading failed.")
              }
          }
        }
        // save time queried so we can't repeat too soon:
    }
}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
