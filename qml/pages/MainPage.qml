/*

Apache License 2.0

Copyright (c) 2022 Peter G. (nephros)

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.  
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page


    Connections {
        onStatusChanged: {
            if (pageStack.busy) {
                pageStack.busyChanged.connect(pushNextPage)
            } else {
                pushNextPage()
            }
        }
    }
    function pushNextPage() {
        if (
            status === PageStatus.Active
            && pageStack.nextPage() === null
            && !pageStack.busy
        ) {
            pageStack.pushAttached("MorsePage.qml");
            pageStack.busyChanged.disconnect(pushNextPage);
        }
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: col.height
        Column {
            id: col
            width: parent.width - Theme.horizontalPageMargin
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: Theme.paddingLarge
            bottomPadding: Theme.itemSizeLarge
            add:      Transition { FadeAnimation { duration: 1200 } }
            move:     Transition { FadeAnimation { duration: 1200 } }
            populate: Transition { FadeAnimation { duration: 1200 } }
            PageHeader { id: head ; title: qsTr("La Dit Dah")
                //TextSwitch { text: checked ? qsTr("sending") : qsTr("idle"); checked: sending; anchors.left: parent.extraContent.left}
            }
            Fresnel {
            height: Theme.itemSizeHuge*2
            }
            SectionHeader { text: qsTr("Signalling Settings") }

            Row {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                TextSwitch {
                    width: parent.width/3
                    text: qsTr("blink")
                    palette.backgroundGlowColor: (wpm > 14) ? "red" : Theme.backgroundGlowColor
                    checked: app.visual
                    automaticCheck: true
                    //enabled: !sending
                    onCheckedChanged: app.visual = checked
                }
                TextSwitch {
                    width: parent.width/3
                    text: qsTr("beep")
                    checked: !app.silent
                    automaticCheck: true
                    //enabled: !sending
                    onCheckedChanged: app.silent = !checked
                }
                TextSwitch {
                    width: parent.width/3
                    text: qsTr("loop")
                    checked: app.loop
                    automaticCheck: true
                    //enabled: false
                    onCheckedChanged: app.loop = checked
                }
            }
            Slider {id: wpmSlider
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                minimumValue: 1
                maximumValue: 23
                stepSize: 1
                label: qsTr("Transmission Rate")
                value: 5
                valueText: value + " WpM"
                onDownChanged: wpm = sliderValue
            }
            Label {
                width: parent.width - Theme.paddingLarge *2
                anchors.horizontalCenter: wpmSlider.horizontalCenter
                text: qsTr("dit: %1ms").arg(ditLength)
                    + " " + qsTr("dah: %1ms").arg(dahLength)
                    + " " + qsTr("sp: %1ms").arg(symbolPause)
                    + " " + qsTr("lp: %1ms").arg(letterPause)
                    + " " + qsTr("wp: %1ms").arg(wordPause)
                horizontalAlignment: Qt.AlignHCenter
                font.pixelSize: Theme.fontSizeTiny
            }
            SectionHeader { text: qsTr("Convert and morse") }
            Row {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                TextArea{ id: inputfield;
                    text:  "Ahoy Sailors!"
                    width: parent.width/2
                    inputMethodHints: Qt.ImhNoPredictiveText
                }
                TextArea{ id: morselabel
                    width: parent.width/2
                    height: Math.max(implicitHeight, inputfield.height)
                    readOnly: true
                    softwareInputPanelEnabled: false
                    inputMethodHints: Qt.ImhNoPredictiveText
                    text: wordToMorse(inputfield.text).join("")
                    font.pixelSize: Theme.fontSizeMedium
                    font.family: "monospace"
                    font.bold: true
                    font.letterSpacing: 3
                    wrapMode: Text.WordWrap
                }
            }
            ButtonLayout {
                width:parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                Button { id: blinkButton
                    text: qsTr("Send!")
                    enabled: !sending && morselabel.text != ""
                    onClicked: send(toMorse(inputfield.text))
                }
            }
        }
        PullDownMenu { id: pdp
            busy: sending
            MenuItem { text: qsTr("About"); onClicked: { pageStack.push(Qt.resolvedUrl("AboutPage.qml")) } }
            //MenuItem { text: qsTr("Settings"); onClicked: { pageStack.push(Qt.resolvedUrl("SettingsPage.qml")) } }
            MenuItem { text: qsTr("Stop Sending"); enabled: sending; onClicked: { loop = false; blinker.stop() } }
        }
        RemorsePopup {
            id: applyRemorse
        }

        VerticalScrollDecorator {}
        //PageBusyIndicator { id: busy ; running: sending; anchors.centerIn: page }
    }

    function send(mstr) { blinker.queue = mstr }

}

// vim: expandtab ts=4 st=4 sw=4 filetype=javascript
