<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>AboutPage</name>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation>Copyright:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation>Källkod:</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation>Erkännanden</translation>
    </message>
    <message>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>Översättningar: %1</translation>
    </message>
    <message>
        <source>What&apos;s %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 is a simple morse code application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>WARNING: the visual blinking mode using the torch may damage your camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Activating the Torch activates the camera, and it is unknown whether doing that in rapid succession may have a negative effect on the hardware. At least the IS actuator will certainly experience wear.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailsPanel</name>
    <message>
        <source>Unit Properties</source>
        <translation type="vanished">Enhetsegenskaper</translation>
    </message>
    <message>
        <source>Service Properties</source>
        <translation type="vanished">Tjänstegenskaper</translation>
    </message>
    <message>
        <source>Timer Properties</source>
        <translation type="vanished">Timeregenskaper</translation>
    </message>
    <message>
        <source>Mount Properties</source>
        <translation type="vanished">Monteringsegenskaper</translation>
    </message>
    <message>
        <source>Path Properties</source>
        <translation type="vanished">Sökvägsegenskaper</translation>
    </message>
    <message>
        <source>Unit name copied to clipboard</source>
        <translation type="vanished">Enhetsnamn kopierat till urklipp</translation>
    </message>
    <message>
        <source>Long press again to copy detailed info</source>
        <translation type="vanished">Långtryck igen för att kopiera detaljerad info</translation>
    </message>
    <message>
        <source>Unit details copied to clipboard</source>
        <translation type="vanished">Enhetsdetaljer kopierat till urklipp</translation>
    </message>
    <message>
        <source>Other Names</source>
        <translation type="vanished">Andra namn</translation>
    </message>
    <message>
        <source>Dependencies</source>
        <translation type="vanished">Beroenden</translation>
    </message>
    <message>
        <source>Unit File</source>
        <translation type="vanished">Enhetsfil</translation>
    </message>
    <message>
        <source>Copy Path</source>
        <translation type="vanished">Kopiera sökväg</translation>
    </message>
    <message>
        <source>Unit file path copied to clipboard</source>
        <translation type="vanished">Enhetsfilens sökväg kopierad till urklipp</translation>
    </message>
    <message>
        <source>Show Content</source>
        <translation type="vanished">Visa innehåll</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation type="vanished">%1 kopierad till urklipp</translation>
    </message>
    <message>
        <source>unit details</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsdetaljer</translation>
    </message>
    <message>
        <source>unit name</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsnamn</translation>
    </message>
    <message>
        <source>unit file path</source>
        <comment>parameter for &apos;copied to clipboard&apos; popup message</comment>
        <translation type="vanished">Enhetsfilens sökväg</translation>
    </message>
</context>
<context>
    <name>Fresnel</name>
    <message>
        <source>Sending.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tap to enter fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>idle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Inställningar</translation>
    </message>
    <message>
        <source>La Dit Dah</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Convert and morse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Transmission Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>blink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop Sending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>dit: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>dah: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sp: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>lp: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>wp: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Signalling Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MorsePage</name>
    <message>
        <source>Morse Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can place custom words here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>by placing a file at %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>page title</comment>
        <translation type="vanished">Inställningar</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">Program</translation>
    </message>
    <message>
        <source>Show Notifications</source>
        <translation type="vanished">Visa aviseringar</translation>
    </message>
    <message>
        <source>List Cleared.</source>
        <translation type="vanished">Listan rensad.</translation>
    </message>
    <message>
        <source>Sticky Notifications</source>
        <translation type="vanished">Fasta aviseringar</translation>
    </message>
    <message>
        <source>Advanced:</source>
        <translation type="vanished">Avancerat:</translation>
    </message>
    <message>
        <source>min</source>
        <translation type="vanished">min</translation>
    </message>
</context>
<context>
    <name>UnitItem</name>
    <message>
        <source>Ignore this session</source>
        <translation type="vanished">Undanta denna session</translation>
    </message>
    <message>
        <source>Ignore permanently</source>
        <translation type="vanished">Undanta permanent</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Starta om</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="vanished">Aktivera</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="vanished">Inaktivera</translation>
    </message>
    <message>
        <source>Mount</source>
        <translation type="vanished">Montera</translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation type="vanished">Avmontera</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Starta</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Stoppa</translation>
    </message>
    <message>
        <source>No actions available.</source>
        <translation type="vanished">Inga åtgärder tillgängliga.</translation>
    </message>
    <message>
        <source>Remount</source>
        <translation type="vanished">Återmontera</translation>
    </message>
    <message>
        <source>next</source>
        <translation type="vanished">nästa</translation>
    </message>
    <message>
        <source>Enabling %1</source>
        <translation type="vanished">Aktiverar %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="vanished">Inaktiverar%1</translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="vanished">Stoppar %1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="vanished">Startar om %1</translation>
    </message>
</context>
<context>
    <name>UnitPage</name>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Inställningar</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Uppdatera</translation>
    </message>
    <message>
        <source>Switch to %1</source>
        <translation type="vanished">Växla till %1</translation>
    </message>
</context>
<context>
    <name>ViewUnitFile</name>
    <message>
        <source>Copy file path to Clipboard</source>
        <translation type="vanished">Kopiera filsökväg till urklipp</translation>
    </message>
    <message>
        <source>Copy contents to Clipboard</source>
        <translation type="vanished">Kopiera innehåll till urklipp</translation>
    </message>
    <message>
        <source>Open…</source>
        <translation type="vanished">Öppna…</translation>
    </message>
    <message>
        <source>Copy %1 to clipboard</source>
        <translation type="vanished">Kopiera %1 till urklipp</translation>
    </message>
    <message>
        <source>file path</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="vanished">filsökväg</translation>
    </message>
    <message>
        <source>%1 copied to clipboard</source>
        <translation type="vanished">%1 kopierad till urklipp</translation>
    </message>
    <message>
        <source>file contents</source>
        <comment>argument for &apos;copy to clipboard&apos; menu entry</comment>
        <translation type="vanished">innehåll</translation>
    </message>
    <message>
        <source>Share %1…</source>
        <translation type="vanished">Dela %1…</translation>
    </message>
</context>
<context>
    <name>harbour-sailord</name>
    <message>
        <source>Enabling %1</source>
        <translation type="vanished">Aktiverar %1</translation>
    </message>
    <message>
        <source>Disabling %1</source>
        <translation type="vanished">Inaktiverar%1</translation>
    </message>
    <message>
        <source>Restarting %1</source>
        <translation type="vanished">Startar om %1</translation>
    </message>
    <message>
        <source>Stopping %1</source>
        <translation type="vanished">Stoppar %1</translation>
    </message>
    <message>
        <source>%1 successful.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation type="vanished">%1 slutfört.</translation>
    </message>
    <message>
        <source>Success:</source>
        <translation type="vanished">Slutfört:</translation>
    </message>
    <message>
        <source>%1 failed.</source>
        <comment>arg is an operation, such as &apos;restarting service X&apos;</comment>
        <translation type="vanished">%1 misslyckades.</translation>
    </message>
    <message>
        <source>Failure:</source>
        <translation type="vanished">Misslyckat:</translation>
    </message>
    <message>
        <source>Preset %1</source>
        <translation type="vanished">Förinställ %1</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Aktivera</translation>
    </message>
    <message>
        <source>Enable %1</source>
        <translation type="vanished">Aktivera %1</translation>
    </message>
    <message>
        <source>Disable</source>
        <translation type="obsolete">Inaktivera</translation>
    </message>
    <message>
        <source>Disable %1</source>
        <translation type="vanished">Inaktivera %1</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="vanished">Starta</translation>
    </message>
    <message>
        <source>Start %1</source>
        <translation type="vanished">Starta %1</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation type="vanished">Stoppa</translation>
    </message>
    <message>
        <source>Stop %1</source>
        <translation type="vanished">Stoppa %1</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Starta om</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <translation type="vanished">Starta om %1</translation>
    </message>
</context>
</TS>
