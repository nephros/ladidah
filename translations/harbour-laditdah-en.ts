<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="42"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>What&apos;s %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <source>%1 is a simple morse code application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>WARNING: the visual blinking mode using the torch may damage your camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="53"/>
        <source>Activating the Torch activates the camera, and it is unknown whether doing that in rapid succession may have a negative effect on the hardware. At least the IS actuator will certainly experience wear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="55"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="56"/>
        <source>Copyright:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>Source Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fresnel</name>
    <message>
        <location filename="../qml/components/Fresnel.qml" line="64"/>
        <source>Sending.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/Fresnel.qml" line="64"/>
        <source>Tap to enter fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/Fresnel.qml" line="64"/>
        <source>idle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <source>La Dit Dah</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="69"/>
        <source>Signalling Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="76"/>
        <source>blink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="85"/>
        <source>beep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="93"/>
        <source>loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="122"/>
        <source>Convert and morse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="149"/>
        <source>Send!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="106"/>
        <source>Transmission Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="114"/>
        <source>dit: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="115"/>
        <source>dah: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="116"/>
        <source>sp: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="117"/>
        <source>lp: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="118"/>
        <source>wp: %1ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="157"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="159"/>
        <source>Stop Sending</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MorsePage</name>
    <message>
        <location filename="../qml/pages/MorsePage.qml" line="45"/>
        <source>Morse Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MorsePage.qml" line="170"/>
        <source>You can place custom words here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MorsePage.qml" line="172"/>
        <source>by placing a file at %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
