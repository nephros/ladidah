<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="42"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="43"/>
        <source>What&apos;s %1?</source>
        <translation>Was ist %1?</translation>
    </message>
    <message>
        <source> </source>
        <translation type="vanished"> </translation>
    </message>
    <message>
        <source>%1 is ..</source>
        <translation type="vanished">%1 ist ...</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="51"/>
        <source>%1 is a simple morse code application.</source>
        <translation> ist eine simple App, um Morse Code zu produzieren und zu senden.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="52"/>
        <source>WARNING: the visual blinking mode using the torch may damage your camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="53"/>
        <source>Activating the Torch activates the camera, and it is unknown whether doing that in rapid succession may have a negative effect on the hardware. At least the IS actuator will certainly experience wear.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="55"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="56"/>
        <source>Copyright:</source>
        <translation>Urheberrecht:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="57"/>
        <source>License:</source>
        <translation>Lizenz:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="58"/>
        <source>Source Code:</source>
        <translation>Quellcode:</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="59"/>
        <source>Credits</source>
        <translation>Danksagungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="61"/>
        <source>Translation: %1</source>
        <comment>%1 is the native language name</comment>
        <translation>%1 Übersetzung</translation>
    </message>
</context>
<context>
    <name>Fresnel</name>
    <message>
        <location filename="../qml/components/Fresnel.qml" line="64"/>
        <source>Sending.</source>
        <translation>Sende.</translation>
    </message>
    <message>
        <location filename="../qml/components/Fresnel.qml" line="64"/>
        <source>Tap to enter fullscreen</source>
        <translation>Tippe um zum Vollbild zu wechseln</translation>
    </message>
    <message>
        <location filename="../qml/components/Fresnel.qml" line="64"/>
        <source>idle</source>
        <translation>inaktiv</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="63"/>
        <source>La Dit Dah</source>
        <translation>La Dit Dah</translation>
    </message>
    <message>
        <source>sending</source>
        <translation type="vanished">sende</translation>
    </message>
    <message>
        <source>idle</source>
        <translation type="vanished">inaktiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="69"/>
        <source>Signalling Settings</source>
        <translation>Signaleinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="76"/>
        <source>blink</source>
        <translation>visuell</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="85"/>
        <source>beep</source>
        <translation>akustisch</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="93"/>
        <source>loop</source>
        <translation>wiederholen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="122"/>
        <source>Convert and morse</source>
        <translation>Umwandeln und Morden</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="149"/>
        <source>Send!</source>
        <translation>Senden!</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="106"/>
        <source>Transmission Rate</source>
        <translation>&quot;Übertragungsrate</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="114"/>
        <source>dit: %1ms</source>
        <translation>Dit: %1ms</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="115"/>
        <source>dah: %1ms</source>
        <translation>Dah: %1ms</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="116"/>
        <source>sp: %1ms</source>
        <translation>SyP</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="117"/>
        <source>lp: %1ms</source>
        <translation>LeP</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="118"/>
        <source>wp: %1ms</source>
        <translation>WoP</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="157"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="159"/>
        <source>Stop Sending</source>
        <translation>Senden Beenden</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Einstellungen</translation>
    </message>
</context>
<context>
    <name>MorsePage</name>
    <message>
        <location filename="../qml/pages/MorsePage.qml" line="45"/>
        <source>Morse Manually</source>
        <translation>Händisch Morsen</translation>
    </message>
    <message>
        <location filename="../qml/pages/MorsePage.qml" line="170"/>
        <source>You can place custom words here.</source>
        <translation>Hier könnten deine Kützel stehen.</translation>
    </message>
    <message>
        <location filename="../qml/pages/MorsePage.qml" line="172"/>
        <source>by placing a file at %1</source>
        <translation>Erstelle eine Datei unter %1</translation>
    </message>
</context>
</TS>
