## La Dit Dah

A simple Morse Code application for Sailfish OS.

Features:
  - translation of input text into Morse Code
  - Sending of said code as either blink signals- audio, or both
  - looping tamsmissions
  - manual morsing

### Disclaimer

WARNING: the visual blinking mode uses the Torch which activates the camera,
and it is unknown whether doing that in rapid succession may have a negative
effect on the hardware.

At least the IS actuator will certainly experience wear.

### Custom word snippets for manual morsing

Place a JSON file at ~/.local/share/org.nephros.sailfish/LaDitDah/snippets.json

It should contain array called "snippets", containing objects with the following properties:

  - name:     a name for the snippet
  - plain:    a plain text string, it will be converted into morse code when activated
  - morse:    a morse code string, consisting of . (dit) and - (dah) characters

`name` is optional, and either `plain`, or `morse` should be defined. If both
are given, `morse` is preferred to `plain` by the app.

An example is given below:
    {
      "snippets": [
        {
          "name": "Sos 1",
          "plain": "CQD",
          "morse": ""
        },
        {
          "name": "Sos 2",
          "plain": "SOS",
          "morse": ""
        },
        {
          "name": "WTF",
          "plain": "",
          "morse": ".---..-."
        }
      ]
    }
